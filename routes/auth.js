const express = require("express")
const router = express.Router()
const User = require('../models/auth')
const nodemailer = require('nodemailer')
const _ = require('lodash')
var jwt = require('jsonwebtoken');
const path = require('path')
var config = require('../config/config');
  require('dotenv').config();
var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD
    },
  });
router.post("/",async(req,res)=>{
    const newuser = new User({
    email : req.body.email,
    password : req.body.password
    })
     const a1 = await newuser.save()
     console.log(a1)
     res.send(a1)
})

router.put('/forgetpassword', function (req, res) {
    const email = req.body.email
    User.findOne({email: email},(err,user)=>{
            //checking if the email address sent by client is present in the db(valid)
           if(err|| !user){
               return res.json({error:"user not exist"})
           }
          const token = jwt.sign({_id:user._id},process.env.RESET_PASSWORD_KEY,{expiresIn:'20m'})
             let mailOptions = {
                            from: 'tmahajan002@gmail.com',
                            to: user.email,
                            subject: 'Reset your account password',
                            html : `<h2> Please Click on the link reset your password.</h2>
                            <p>${process.env.CLIENT_URL}/${token}</p>`	
                         }
                         return user.updateOne({resetlink: token},function(err,success){
                             if(err){
                                 return res.sendStatus(400).json({error:"reset password link error"})
                             }else{
                                console.log(mailOptions)
                                let mailSent = smtpTransport.sendMail(mailOptions)//sending mail to the user where he can reset password.User id and the token generated are sent as params in a link
                                if (mailSent) {
                                    return res.json({success: true, message: 'Check your mail to reset your password.'})
                                } else {
                                    return throwFailed(error, 'Unable to send email.');
                                }
                             }
                         })
                         
    })
                })
                
 router.post('/resetpassword',function(req,res){
     const {resetlink,newPass}=req.body;
     console.log(resetlink,newPass)
     
    //res.json({resetlink,newPass})
     if(resetlink){
       jwt.verify(resetlink,process.env.RESET_PASSWORD_KEY,function(error,decodeData){
           if(error){
            console.log("token expired")
               return res.status(401).json({
                   error:"Incorrect token or it is expired",
                })
               
           }
           User.findOne({resetlink},(err,user)=>{
            if(err|| !user){
                return res.json({error:"user with this token not exist"})
            }
            const obj = {
                password:newPass
            }
            user=_.extend(user,obj);
            user.save((err,result)=>{
                if(err){
                    return res.sendStatus(400).json({error:"reset password error"})
                }else{
                       return res.json({success: true, message: 'your password has been changed.'})
                    
                }  
            })
           })
       })
     }else{
         return res.json({error:"authentication eror"})
    }
 }) 
         

module.exports = router
const express = require("express")
const mongoose = require("mongoose")
const app = express()
const dotenv= require('dotenv').config()


app.use(express.static('public'))
const DBHOST = process.env.DBHOST
const DBNAME = process.env.DBNAME 
//connect to database AlienDb
mongoose.connect(`mongodb://${DBHOST}/${DBNAME}`);
//to hold a connection
const db = mongoose.connection

//on is event it is fire once you connect with database
db.on('open',function(){
    console.log('connected..')
})
app.use(express.json())
app.get('/forget/:token',(req,res)=>{
    console.log(req.params.token)
    res.sendFile(__dirname+'/public/forget.html')
}) 

const authRoutes = require("./routes/auth")
app.use('/',authRoutes)
app.listen(process.env.PORT)

 module.exports = app;